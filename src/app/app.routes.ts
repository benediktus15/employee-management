import { Routes } from '@angular/router';

import { authRoutes } from './pages/auth/auth.routes';
import { adminRoutes } from './pages/admin/admin.routes';
import { authGuard } from './services/auth/auth.guard';

export const routes: Routes = [
  { path: '', redirectTo: 'auth', pathMatch: 'full' },
  { path: 'auth', children: authRoutes },
  { path: 'admin', children: adminRoutes },
  { path: '**', redirectTo: 'auth', pathMatch: 'full' }
];

import { Routes } from '@angular/router';

import { AdminComponent } from './admin.component';
import { DashboardComponent } from './dashboard/dashboard.component';

import { employeeRoutes } from './employee/employee.routes';
import { authGuard } from '../../services/auth/auth.guard';

export const adminRoutes: Routes = [
  {
    path: '',
    component: AdminComponent,
    canActivate: [authGuard],
    children: [
      { path: '', redirectTo: 'employee', pathMatch: 'full' },
      {
        path: 'dashboard',
        canActivateChild: [authGuard],
        component: DashboardComponent,
      },
      {
        path: 'employee',
        canActivateChild: [authGuard],
        children: employeeRoutes,
      },
    ],
  },
];

import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormControl, FormsModule, NgForm, ReactiveFormsModule } from '@angular/forms';

import { MatSelectModule, MatSelect } from '@angular/material/select';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';

import { IEmployee, IGroups } from '../../../../services/employee/employee.interface';
import { EmployeeService } from '../../../../services/employee/employee.service';
import { ReplaySubject } from 'rxjs';

// TODO: search groups

@Component({
  selector: 'app-employee-add',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    MatSelectModule,
    NgxMatSelectSearchModule,
    ReactiveFormsModule,
  ],
  templateUrl: './add.component.html',
  styleUrl: './add.component.scss',
})
export class EmployeeAddComponent implements OnInit {
  form: IEmployee;
  groups: Array<IGroups>;

  // TODO: select with search
  // groupCtrl: FormControl<Groups> = new FormControl<Groups>(null);
  // groupFilterCtrl: FormControl<string> = new FormControl<string>('');
  // filteredGroups: ReplaySubject<Array<Groups>> = new ReplaySubject<
  //   Array<Groups>
  // >('Group A');
  // @ViewChild('singleSelect', { static: true }) singleSelect: MatSelect;

  constructor(
    private router: Router,
    private employeeService: EmployeeService
  ) {
    this.form = {
      username: '',
      firstName: '',
      lastName: '',
      email: '',
      birthDate: '',
      basicSalary: 0,
      status: '',
      group: '',
      description: '',
    };

    // this.form = {
    //   username: 'ben',
    //   firstName: 'Benediktus',
    //   lastName: 'Zebua',
    //   email: 'benedyctoes@gmail.com',
    //   birthDate: '2024-02-23',
    //   basicSalary: 2000000,
    //   status: 'Active',
    //   group: 'Group C',
    //   description: 'Haii i am ben',
    // };
    this.groups = [
      { id: 'Group A', name: 'Group A' },
      { id: 'Group B', name: 'Group B' },
      { id: 'Group C', name: 'Group C' },
      { id: 'Group D', name: 'Group D' },
      { id: 'Group E', name: 'Group E' },
      { id: 'Group F', name: 'Group F' },
      { id: 'Group G', name: 'Group G' },
      { id: 'Group H', name: 'Group H' },
      { id: 'Group I', name: 'Group I' },
      { id: 'Group J', name: 'Group J' },
    ];
  }

  ngOnInit() {}

  async onSubmit(f: NgForm) {
    if (f.valid === false) return;

    await this.employeeService.addEmployee(f.value).subscribe((response) => {
      this.router.navigateByUrl('/admin/employee');
    });
  }

  onCancel() {
    this.router.navigateByUrl('/admin/employee');
  }
}

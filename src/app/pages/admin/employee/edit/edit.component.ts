import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { FormsModule, NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import {
  IEmployee,
  IGroups,
} from '../../../../services/employee/employee.interface';
import { EmployeeService } from '../../../../services/employee/employee.service';

@Component({
  selector: 'app-employee-edit',
  standalone: true,
  imports: [CommonModule, FormsModule],
  templateUrl: './edit.component.html',
  styleUrl: './edit.component.scss',
})
export class EmployeeEditComponent {
  employee: IEmployee | undefined;
  form: IEmployee;
  groups: Array<IGroups>;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private employeeService: EmployeeService
  ) {
    this.form = {
      username: '',
      firstName: '',
      lastName: '',
      email: '',
      birthDate: '',
      basicSalary: 0,
      status: '',
      group: '',
      description: '',
    };

    this.groups = [
      { id: 'Group A', name: 'Group A' },
      { id: 'Group B', name: 'Group B' },
      { id: 'Group C', name: 'Group C' },
      { id: 'Group D', name: 'Group D' },
      { id: 'Group E', name: 'Group E' },
      { id: 'Group F', name: 'Group F' },
      { id: 'Group G', name: 'Group G' },
      { id: 'Group H', name: 'Group H' },
      { id: 'Group I', name: 'Group I' },
      { id: 'Group J', name: 'Group J' },
    ];

    this.fetchEmployee();
  }

  get id(): string {
    let val = '';
    this.route.params.subscribe((params) => {
      val = params['id'];
    });
    return val;
  }

  async fetchEmployee(): Promise<void> {
    await this.employeeService.getEmployee(this.id).subscribe((v) => {
      this.employee = v;
      if (v !== undefined) this.form = v;
    });
  }

  async onSubmit(f: NgForm) {
    if (f.valid === false) return;

    await this.employeeService.editEmployee(this.employee?.id!, f.value).subscribe((response) => {
      this.router.navigateByUrl('/admin/employee');
    });
  }

  onCancel() {
    this.router.navigateByUrl('/admin/employee');
  }
}

import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { EmployeeService } from '../../../../services/employee/employee.service';
import { IEmployee } from '../../../../services/employee/employee.interface';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-employee-view',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './view.component.html',
  styleUrl: './view.component.scss',
})
export class EmployeeViewComponent {
  employee: IEmployee | undefined;

  constructor(
    private route: ActivatedRoute,
    private employeeService: EmployeeService
  ) {
    this.employee = undefined;
    this.fetchEmployee();
  }

  get id(): string {
    let val = '';
    this.route.params.subscribe((params) => {
      val = params['id'];
    });
    return val;
  }

  async fetchEmployee(): Promise<void> {
    await this.employeeService.getEmployee(this.id).subscribe((v) => {
      this.employee = v;
    });
  }
}

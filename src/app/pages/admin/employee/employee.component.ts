import { Component, ViewChild } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router } from '@angular/router';

import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';

import { EmployeeService } from '../../../services/employee/employee.service';
import { IEmployee } from '../../../services/employee/employee.interface';

// TODO: search feature

@Component({
  selector: 'app-employee',
  standalone: true,
  imports: [
    CommonModule,
    MatButtonModule,
    MatIconModule,
    MatTableModule,
    MatPaginatorModule,
  ],
  templateUrl: './employee.component.html',
  styleUrl: './employee.component.scss',
  providers: [EmployeeService],
})
export class EmployeeComponent {
  employees: Array<IEmployee> = [];
  dataSource = new MatTableDataSource<IEmployee>([]);
  displayedColumns: string[] = [
    'no',
    'username',
    'firstName',
    'lastName',
    'email',
    'status',
    'action',
  ];

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor(
    private router: Router,
    private _snackBar: MatSnackBar,
    private employeeService: EmployeeService,
  ) {
    this.init();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  async init() {
    this.fetchEmployee();
  }

  async fetchEmployee() {
    await this.employeeService.getListEmployee().subscribe((respnonse) => {
      this.employees = respnonse;
      this.dataSource = new MatTableDataSource<IEmployee>(this.employees);
    });
  }

  addEmployee() {
    this.router.navigate(['admin/employee/add']);
  }

  detailEmployee(id: string) {
    this.router.navigate(['admin/employee/view', id]);
  }

  editEmployee(id: string) {
    this.router.navigate(['admin/employee/edit', id]);
  }

  async deleteEmployee(id: string) {
    await this.employeeService.deleteEmployee(id).subscribe((s) => {
      this.fetchEmployee();
      this._snackBar.open('Employee data deleted');
    });
  }
}

import { Routes } from '@angular/router';
import { EmployeeComponent } from './employee.component';
import { EmployeeAddComponent } from './add/add.component';
import { EmployeeEditComponent } from './edit/edit.component';
import { EmployeeViewComponent } from './view/view.component';

export const employeeRoutes: Routes = [
  { path: '', component: EmployeeComponent },
  { path: 'add', component: EmployeeAddComponent },
  { path: 'edit/:id', component: EmployeeEditComponent },
  { path: 'view/:id', component: EmployeeViewComponent },
];

import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../../services/auth/auth.service';

@Component({
  selector: 'app-auth-login',
  standalone: true,
  imports: [CommonModule, FormsModule],
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss',
})
export class AuthLoginComponent {
  form = {
    username: 'admin',
    password: 'admin123',
  };
  loading = false;

  constructor(public authService: AuthService, public router: Router) {}

  login(f: NgForm) {
    if (f.valid === false) return;

    this.loading = true;
    this.authService.login(f.value).subscribe((v) => {
      this.loading = false;
      if (v === true) {
        if (this.authService.isLoggedIn) {
          const redirectUrl = '/admin';
          this.router.navigate([redirectUrl]);
        }
      }
    });
  }

  submit() {
    console.log('submit');
  }
}

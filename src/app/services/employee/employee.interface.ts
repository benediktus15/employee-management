export interface IEmployee {
  id?: string;
  avatar?: string;
  username: string;
  firstName: string;
  lastName: string;
  email: string;
  birthDate: string;
  basicSalary: number;
  status: string;
  group: string;
  description: string;
}

export interface IGroups {
  id: string;
  name: string;
}
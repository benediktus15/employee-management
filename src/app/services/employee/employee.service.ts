import { Injectable } from '@angular/core';
import { IEmployee } from './employee.interface';
import { Observable, mergeMap, of, tap } from 'rxjs';
import { find, findIndex, remove } from 'lodash';
import { dataEmployee } from './employee.data';

@Injectable({
  providedIn: 'root',
})
export class EmployeeService {
  constructor() {}

  getListEmployee(): Observable<Array<IEmployee>> {
    return of(true).pipe(
      mergeMap(() => {
        // get employee data form local storage
        const results = localStorage.getItem('employees') as string;

        if (results !== null || results !== undefined) {
          const employees = JSON.parse(results) as Array<IEmployee> || [];
          if (employees?.length > 0) {
            return of(employees);
          } else {
            localStorage.setItem('employees', JSON.stringify(dataEmployee));
            return of(dataEmployee);
          }
        } else {
          return of([]);
        }
      })
    );
  }

  getEmployee(id: string): Observable<IEmployee | undefined> {
    return of(true).pipe(
      mergeMap(() => {
        // get employee data form local storage
        const results = localStorage.getItem('employees') as string;

        const employees = JSON.parse(results) as Array<IEmployee>;
        const employee = find(employees, (obj) => obj.id === id);
        return of(employee);
      })
    );
  }

  addEmployee(data: IEmployee): Observable<IEmployee> {
    data = { ...data, id: (+new Date()).toString() };

    return of(true).pipe(
      tap(() => {
        // get employee data form local storage
        const results = localStorage.getItem('employees') as string;

        if (results !== null && results !== undefined) {
          const employees = JSON.parse(results) as Array<IEmployee>;

          employees.push(data);
          localStorage.setItem('employees', JSON.stringify(employees));
        } else {
          localStorage.setItem('employees', JSON.stringify([data]));
        }
      }),
      mergeMap(() => of(data))
    );
  }

  editEmployee(id: string, data: IEmployee): Observable<IEmployee> {
    return of(true).pipe(
      tap(() => {
        // get employee data form local storage
        const results = localStorage.getItem('employees') as string;

        if (results !== null && results !== undefined) {
          const employees = JSON.parse(results) as Array<IEmployee>;

          const modifiedEmployees = employees.map((obj) => {
            if (obj.id === id) {
              return { ...obj, ...data };
            }
            return obj;
          });

          localStorage.setItem('employees', JSON.stringify(modifiedEmployees));
        }
      }),
      mergeMap(() => of(data))
    );
  }

  deleteEmployee(id: string): Observable<boolean> {
    return of(true).pipe(
      tap(() => {
        // get employee data form local storage
        const results = localStorage.getItem('employees') as string;

        if (results !== null && results !== undefined) {
          let employees = JSON.parse(results) as Array<IEmployee>;

          const employeesIndex = findIndex(employees, (obj) => obj.id === id);

          if (employeesIndex > -1) {
            employees.splice(employeesIndex, 1);
            localStorage.setItem('employees', JSON.stringify(employees));
          }
        }
      })
    );
  }
}

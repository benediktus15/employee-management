import { Injectable } from '@angular/core';
import { Observable, delay, mergeMap, of, tap } from 'rxjs';
import { ILogin } from './auth.interface';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  isLoggedIn = false;

  // store the URL so we can redirect after logging in
  redirectUrl: string | null = null;

  session(): void {
    const isLoggedInLocal = localStorage.getItem('isLoggedIn');
    this.isLoggedIn = isLoggedInLocal === 'true' ? true : false;
  }

  login(data: ILogin): Observable<boolean> {
    return of(true).pipe(
      delay(1000),
      mergeMap(() => {
        if (
          data.username.toLowerCase().trim() === 'admin' &&
          data.password === 'admin123'
        ) {
          this.isLoggedIn = true;
          localStorage.setItem('isLoggedIn', 'true');
          return of(true);
        } else {
          return of(false);
        }
      })
    );
  }

  logout(): Observable<boolean> {
    return of(true).pipe(
      delay(1000),
      tap(() => {
        this.isLoggedIn = false;
        localStorage.removeItem('isLoggedIn');
      })
    );
  }
}
